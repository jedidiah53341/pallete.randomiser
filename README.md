# pallete.randomiser

![pallete.randomiser.screenshot](sc.jpg)
A simple HTML meant to automatically generate random color schemes. By checking the 'automate' box, a new combination is made every 3 seconds. I hope that, as users look at it, they would sometimes spot a good color combination as a reference.
